#include<iostream>

using namespace std;

template <class T>                                                                                        	//Declaring template.
void swap_values(T& v1, T& v2);																			 	//swap function.
template <class T>
T smallest_index(const T a[], int start_index, int number_used);											//smallest index template.
template <class T>
void sort(T a[], int number_used);																			//sort function.


//Demonstration of sorting using template.
int main() 
{
	const int size = 10;
	int x_int[size];
	char x_char[size];
	double x_doub[size];

	cout << "*****INTEGERS SORTING*****" << endl;
	cout << "    Input 10 integers." << endl;

	for (int j = 0; j<size; j++)
	{
		cin >> x_int[j];																					
	}
	sort(x_int, size);
	cout << "Sorted : " << endl;																			//display sorted integers.

	for (int i = 0; i<size; i++)
	{
		cout << x_int[i] << " ";
	}
	cout << endl << "***********************";
	cout << endl << endl;

	cout << "*****DOUBLES SORTING*****" << endl;
	cout << "    Input 10 doubles." << endl;

	for (int j = 0; j<size; j++)
	{
		cin >> x_doub[j];
	}
	sort(x_doub, size);
	cout << "Sorted : " << endl;																			//display sorted doubles.

	for (int i = 0; i<size; i++)
	{
		cout << x_doub[i] << " ";
	}
	cout << endl << "***********************";
	cout << endl << endl;

	cout << "*****CHARACTORS SORTING*****" << endl;
	cout << "    Input 10 charactors." << endl;

	for (int j = 0; j<size; j++)
	{
		cin >> x_char[j];
	}
	sort(x_char, size);
	cout << "Sorted : " << endl;																			//display sorted characters.

	for (int i = 0; i<size; i++)
	{
		cout << x_char[i] << " ";
	}
	cout << endl << "***********************";
	cout << endl << endl;
}


template <class T>																							//template swap function.
void swap_values(T& v1, T& v2)
{
	T tmp;
	tmp = v1;
	v1 = v2;
	v2 = tmp;
}

template <class T>																							//sorting template.
T smallest_index(const T a[], int start_index, int number_used)
{
	T min = a[start_index];
	int index_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_min = index;
		}
	return index_min;
}

template <class T>																							//sorting template.
void sort(T a[], int number_used)
{
	int next_smallest_index;
	for (int index = 0; index < number_used - 1; index++)
	{
		next_smallest_index = smallest_index(a, index, number_used);
		swap_values(a[index], a[next_smallest_index]);
	}
}
